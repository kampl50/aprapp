import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UserStartComponent } from './user/user-start/user-start.component';
import { WorkerComponent } from './worker/worker-start/worker-start.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login/login-routing.module';
import { AccountComponent } from './user/account/account.component';
import { TakeOrderComponent } from './user/take-order/take-order.component';
import { ListOrderComponent } from './user/list-order/list-order.component';

import { ButtonModule } from 'primeng/button';
import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from 'primeng/calendar';
import { ListboxModule } from 'primeng/listbox';
import { OrdersComponent } from './worker/orders/orders.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { EditWorkerOrderComponent } from './worker/edit-worker-order/edit-worker-order.component';
import { CardModule } from 'primeng/card';
import { NotWorkerOrdersComponent } from './worker/not-worker-orders/not-worker-orders.component';
import { AccountWorkerComponent } from './worker/account-worker/account-worker.component';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserStartComponent,
    WorkerComponent,
    AccountComponent,
    TakeOrderComponent,
    ListOrderComponent,
    OrdersComponent,
    OrderDetailsComponent,
    EditWorkerOrderComponent,
    NotWorkerOrdersComponent,
    AccountWorkerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    LoginRoutingModule,
    ButtonModule,
    TabMenuModule,
    TableModule,
    DropdownModule,
    InputTextareaModule,
    CalendarModule,
    ListboxModule,
    CardModule,
    MessagesModule,
    MessageModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
