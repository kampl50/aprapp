import { Injectable } from '@angular/core';
import { IUser } from '../models/interfaces/IUser';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  users: IUser[];
  constructor() {
    this.loadData();
  }

  public updateUser(updatedUser: IUser) {
    this.users.forEach((user, index) => {
      if (user.id === updatedUser.id) {
        this.users.splice(index, 1, updatedUser);
      }
    });
  }

  public deleteUser(userToDelete: IUser) {
    this.users.forEach((user, index) => {
      if (user === userToDelete) this.users.splice(index, 1);
    });
  }

  public getUsers(): Array<IUser> {
    return this.users;
  }

  private loadData() {
    this.users = [
      {
        id: 1,
        login: 'user1',
        password: 'user1',
        role: 'USER',
        name: 'Thomas',
        surname: 'Anders',
        email: 'thomas@gmail.com',
        phone: '608289351',
      },
      {
        id: 2,
        login: 'user2',
        password: 'user2',
        role: 'USER',
        name: 'Carlito',
        surname: 'Bandito',
        email: 'calito@gmail.com',
        phone: '723432321',
      },
    ];
  }
}
