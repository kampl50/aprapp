import { Injectable } from '@angular/core';
import { IWorker } from 'src/app/models/interfaces/IWorker';

@Injectable({
  providedIn: 'root',
})
export class WorkerService {
  workers: IWorker[];
  constructor() {
    this.loadData();
    console.log(this.workers);
  }

  public updateWorkers(updatedWorker: IWorker) {
    this.workers.forEach((user, index) => {
      if (user.id === updatedWorker.id) {
        this.workers.splice(index, 1, updatedWorker);
      }
    });
  }

  public deleteWorkers(workerToDelete: IWorker) {
    this.workers.forEach((user, index) => {
      if (user === workerToDelete) this.workers.splice(index, 1);
    });
  }

  public getWorkers(): Array<IWorker> {
    return this.workers;
  }

  private loadData() {
    this.workers = [
      {
        id: 1,
        login: 'worker1',
        password: 'worker1',
        role: 'WORKER',
        name: 'Jaroslaw',
        surname: 'Syrek',
        email: 'jaroslaw@gmail.com',
        phone: '5436457654',
        isBusy: false,
        experience: 5,
        preferMark: 'OPEL',
      },
      {
        id: 2,
        login: 'worker2',
        password: 'worker2',
        role: 'WORKER',
        name: 'Gosia',
        surname: 'Fajnistka',
        email: 'kozuchowska@gmail.com',
        phone: '243564364',
        isBusy: false,
        experience: 8,
        preferMark: 'AUDI',
      },
    ];
  }
}
