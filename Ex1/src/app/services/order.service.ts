import { IOrder } from '../models/interfaces/IOrder';
import { Injectable } from '@angular/core';
import { Mark } from '../models/enums/Mark';
import { EngineKind } from '../models/enums/EngineKind';
import { OrderKind } from '../models/enums/OrderKind';
import { OrderStatus } from '../models/enums/OrderStatus';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  orders: IOrder[];

  constructor() {
    this.loadData();
  }

  public deleteUserOrder(id: number) {
    this.orders.forEach((order, index) => {
      if (id === order.id) this.orders.splice(index, 1);
    });
  }

  public unsubscribeOrderByWorker(id: number) {
    this.orders.map((order) => {
      if (id === +order.id) {
        order.workersId = -1;
      }
    });
  }

  public updateOrder(updatedOrder: IOrder) {
    this.orders.forEach((order, index) => {
      if (order.id === updatedOrder.id) {
        this.orders.splice(index, 1, updatedOrder);
      }
    });
  }

  public deleteByUserIdOrder(id: number) {
    this.orders.forEach((order, index) => {
      if (id === order.clientId) this.orders.splice(index, 1);
    });
  }

  public addOrder(newOrder: IOrder) {
    this.orders.push(newOrder);
  }

  public getOrders(): Array<IOrder> {
    return this.orders;
  }

  public getIndexLastElement(): number {
    return this.orders.length;
  }

  private loadData() {
    this.orders = [
      {
        id: 1,
        clientId: 1,
        workersId: 1,
        mark: Mark.HYUNDAI,
        engineKind: EngineKind.DIESEL,
        power: 200,
        visitDate: new Date('2020-08-22'),
        orderDescription: 'Zbyt duża ilość czarnego dymu z wydechu',
        orderKind: OrderKind.OVERVIEW,
        orderStatus: OrderStatus.NEW,
        price: 0,
      },
      {
        id: 2,
        clientId: 1,
        workersId: 1,
        mark: Mark.AUDI,
        engineKind: EngineKind.PETROL_WITH_LPG,
        power: 320,
        visitDate: new Date('2021-08-22'),
        orderDescription: 'Uszczelka pod głowicą',
        orderKind: OrderKind.REPAIR,
        orderStatus: OrderStatus.READY,
        price: 1000,
      },
      {
        id: 3,
        clientId: 1,
        workersId: 1,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Silnik nierówno pracuje',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 2500,
      },
      {
        id: 4,
        clientId: 1,
        workersId: 1,
        mark: Mark.CHEVROLET,
        engineKind: EngineKind.HYBRID,
        power: 120,
        visitDate: new Date('2021-09-12'),
        orderDescription: 'Problem z układem sterowania',
        orderKind: OrderKind.REPAIR,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 3500,
      },
      {
        id: 5,
        clientId: 1,
        workersId: 1,
        mark: Mark.MERCEDES,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Klapa bagażnika nie domyka się',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 3300,
      },
      {
        id: 6,
        clientId: 1,
        workersId: 1,
        mark: Mark.PEUGEOT,
        engineKind: EngineKind.PETROL,
        power: 110,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Wymiana klocków hamulcowych',
        orderKind: OrderKind.EXCHANGE_PART,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 1000,
      },
      {
        id: 7,
        clientId: 1,
        workersId: 1,
        mark: Mark.TOYOTA,
        engineKind: EngineKind.DIESEL,
        power: 220,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Wymiana oleju',
        orderKind: OrderKind.EXCHANGE_PART,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 8,
        clientId: 1,
        workersId: 1,
        mark: Mark.BMW,
        engineKind: EngineKind.PETROL,
        power: 120,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Wymiana skrzyni biegów',
        orderKind: OrderKind.EXCHANGE_PART,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 5500,
      },
      {
        id: 9,
        clientId: 1,
        workersId: 1,
        mark: Mark.BMW,
        engineKind: EngineKind.PETROL,
        power: 120,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Wymiana rozrządu',
        orderKind: OrderKind.EXCHANGE_PART,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 5500,
      },
      {
        id: 10,
        clientId: 1,
        workersId: 1,
        mark: Mark.VOLKSWAGEN,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Niepokojące odgłosy ze skrzyni biegów',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 1000,
      },
      {
        id: 11,
        clientId: 1,
        workersId: 1,
        mark: Mark.VOLVO,
        engineKind: EngineKind.PETROL_WITH_LPG,
        power: 220,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Ustawienie zbieżności',
        orderKind: OrderKind.REPAIR,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 12,
        clientId: 1,
        workersId: 1,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Silnik nierówno pracuje',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 13,
        clientId: 2,
        workersId: 2,
        mark: Mark.TOYOTA,
        engineKind: EngineKind.DIESEL,
        power: 150,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Wymiana przednich lamp',
        orderKind: OrderKind.EXCHANGE_PART,
        orderStatus: OrderStatus.NEW,
        price: 0,
      },
      {
        id: 14,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Pasek rozrządu uszkodzony',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 15,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Wycieraczki zbyt szybko się zużywają',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 16,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Dziwne odłgosy ze skrzyni biegów',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 17,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Czerowna kontrolka od oleju się pali',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 18,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Korbowód wyszedł bokiem.',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 19,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Coś cieknie z chłodnicy',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },

      {
        id: 20,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Silnik nierówno pracuje',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },

      {
        id: 21,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Spalona lewa przednia lampa',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 22,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Tarcze hamulcowe grzeją się.',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 23,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Oberwanie układu wydechowego',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 24,
        clientId: 2,
        workersId: 2,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Sprzęgło się ślizga',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 25,
        clientId: 2,
        workersId: -1,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Drugi bieg cięzko wchodzi',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },

      {
        id: 26,
        clientId: 2,
        workersId: -1,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Sprzęgło zapada się',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },

      {
        id: 27,
        clientId: 2,
        workersId: -1,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Duże luzy na kierownicy',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 28,
        clientId: 2,
        workersId: -1,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Kapeć w tylnich kołach',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 29,
        clientId: 2,
        workersId: -1,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Klimatyzacja nie działa',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
      {
        id: 30,
        clientId: 2,
        workersId: -1,
        mark: Mark.PORSCHE,
        engineKind: EngineKind.PETROL,
        power: 520,
        visitDate: new Date('2020-09-12'),
        orderDescription: 'Gałka skrzyni biegów została złamana',
        orderKind: OrderKind.DIAGNOSTICS,
        orderStatus: OrderStatus.IN_PROGRESS,
        price: 8900,
      },
    ];
  }
}
