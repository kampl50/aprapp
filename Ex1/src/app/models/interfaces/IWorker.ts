export interface IWorker {
  id: number;
  login: string;
  password: string;
  role: string;
  name: string;
  surname: string;
  email: string;
  phone: string;
  isBusy: boolean;
  experience: number;
  preferMark: string;
}
